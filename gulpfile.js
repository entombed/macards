"use strict";

const gulp = require("gulp"),
  pug = require("gulp-pug"),
  watch = require("gulp-watch"),
  plumber = require('gulp-plumber'),
  notify = require('gulp-notify');

const paths = {
  pugFiles: ['./src/**/*.pug'],
  htmlFiles: ['./src/app/**/*.html']
};

const errorHandler = {
  errorHandler: notify.onError({
      title: 'Ошибка в плагине <%= error.plugin %>',
      message: "Описание: <%= error.message %>"
  })
};

function rebuildHtmlFn(...args) {
  return gulp.series(clearHtmlFn, buildHtmFileslFn)(...args);
}

function clearHtmlFn() {
  return del(paths.htmlFiles).then((data) => {
      data.forEach((item) => {
          console.log(`Delete - ${item}`);
      });
  }).catch((error) => {
      console.log(error);
  });
}

function buildHtmFileslFn() {
  return gulp.src(paths.pugFiles)
      .pipe(plumber(errorHandler))
      .pipe(pug({ pretty: true, doctype: 'html' }))
      .pipe(gulp.dest((file) => {
          console.log(`Render - ${file.path}`);
          return file.base;
      }))
}

gulp.task('build:html', rebuildHtmlFn);

gulp.task('whats-new', () => {
  watch(paths.pugFiles).on(
    ('change'), (file) => {
      gulp.src(file)
          .pipe(plumber(errorHandler))
          .pipe(pug({ pretty: true, doctype: 'html' }))
          .pipe(gulp.dest((file) => {
              return file.base;
      }))
  });
})


