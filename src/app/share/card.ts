export interface Card {
  name: string;       // - имя файла номер + расширение (`${i}.jpg`)
  path: string;       // - путь к директории где хранятся файлы
  shirt?: string;     // - имя файла для рубашки ('shirt.png')
  height?: string;    // - высота ('200px')
  type?: string;      // - тип карты (картинка или слова)
  numColor?: string;  // - цвет шлейки с номером карты
  position?: {
    x: number,
    y: number
  };
  locked?: boolean;   // - используется для блокировка во время перемещения
  inverted?: boolean; // - перевернута карта или нет (рубашка вверх или картинка)
  moved: boolean;     // - отслеживать перемещение
  id: number;         // - id карты
  block: string;      // - имя блока карточек, используется что бы отследить куда ее возвращать
}

export interface ParamsButton {
  resort: boolean;    // - кнопка пересортировать
  inverte: boolean;   // - перевернуть карты
}

export class KeyCode {
  public esc = 'Escape';
  public enter = 'Enter';
  public numPadEnter = 'NumpadEnter';
}
