import { trigger, style, transition, animate, keyframes, query, stagger, state } from '@angular/animations';

export const AnimationInDeck =
  trigger('pushToDesk', [
    transition(':leave', [
      animate(200, keyframes([
        style({ transform: 'rotateX(0deg) rotateY(0deg) rotateZ(0deg)', offset: 0 }),
        style({ transform: 'rotateX(0deg) rotateY(90deg) rotateZ(0deg)', offset: 0.5 }),
        style({ transform: 'rotateX(0deg) rotateY(0deg) rotateZ(0deg)', offset: 1 })
      ]))
    ]),
    transition(':enter', [
      animate(1000, keyframes([
        style({ opacity: 0, transform: 'rotateX(0deg) rotateY(0deg) rotateZ(0deg)', offset: 0 }),
        style({ opacity: 0.5, transform: 'rotateX(90deg) rotateY(0deg) rotateZ(0deg)', offset: 0.5 }),
        style({ opacity: 1, transform: 'rotateX(0deg) rotateY(0deg) rotateZ(0deg)', offset: 1 })
      ]))
    ])
  ]);

export const AnimationOnDesk =
  trigger('outFromDesk', [
    transition(':leave', [
      animate(300, keyframes([
        style({ opacity: 1, offset: 0 }),
        style({ opacity: .5, offset: 0.3 }),
        style({ opacity: 0, offset: 1.0 }),
      ]))
    ])
  ]);
