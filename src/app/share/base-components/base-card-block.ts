import { OnInit, Input, Output, EventEmitter, OnDestroy, Directive } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card, ParamsButton } from '@share/card';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

// тип карты, изображение или слова
type CardType = 'image' | 'word';

/**
 * дефолтный объект с которого создаются карточки
 * tmpCard = {
 *  name: `${i}.jpg`,              //- имя файла
 *  path: 'about-me/head',         //- путь к директории где хранятся файлы ( добавляется к /assets/images/ )
 *  height: '200px',               //- высота карты когда она извлечена из колоды и добавлена на стол
 *  shirt: 'shirt.png',            //- картинка рубашки
 *  inverted: false,               //- перевернуты карты или нет
 *  moved: false,                  //- отслеживать перемещение
 *  id: i - 1,                     //- id карты
 *  block: 'app-about-me-block-01' //- имя блока карточек, используется что бы отследить куда ее возвращать
 * }
 *
 * @author A.Bondarenko
 * @date 2019-11-25
 * @export
 * @class BaseCardBlock
 * @implements {OnInit}
 */
@Directive()
export class BaseCardBlock implements OnInit, OnDestroy {
  @Input() random: boolean;
  @Output() outCardToDesk = new EventEmitter<any>();

  // кол-во карт в колоде
  public maxCardCount: number;
  // номера карт которые необходимо пропустить
  public excludeCards: number[] = [];
  // массив карт
  public deckCardsArray: Card[] = [];
  // отображать лицевую сторону или нет
  public showFrontCard = false;
  // заголовок аккардион блока (первая часть)
  public title = null;
  // массив слов для которых нужно будет получить перевод
  public words: string[] = [];
  // сохраненные переводы
  public dictionary: object | void = null;
  // отображать кнопки на панели или нет
  public paramsButton: ParamsButton = {
    resort: true,
    inverte: true
  };
  // тип карты, нужно что бы на полотне различать это изображение или слова
  public cardType: CardType = 'image';
  // тут храним подписки, что бы отписаться при удалении компонента
  protected subscriptions: Subscription = new Subscription();

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    // translate.setDefaultLang('ru');
    // translate.use('ru');
  }

  ngOnInit() {
    if (this.random) {
      this.resort(this.deckCardsArray);
    }
    // если нет рубашки у карточек, прячем кнопку перевернуть
    if (!this.deckCardsArray[0].hasOwnProperty('shirt')) {
      this.paramsButton.inverte = false;
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  /**
   * забираем переводы
   *
   * @author A.Bondarenko
   * @date 2019-11-27
   * @protected
   * @param {string[]} arrayWords - массив слов которые необходимо перевести
   * @returns {(Promise<object | void>)} - объект содержащий переводы (КЛЮЧ: переведенное слово, ...) или ошибку
   * @memberof BaseCardBlock
   */
  protected getTranslates(arrayWords: string[]): Promise<object | void> {
    return this.translate.get(arrayWords).toPromise();
  }

  /**
   *
   *
   * @author A.Bondarenko
   * @date 2019-11-27
   * @protected
   * @param {number} count - кол-во карт
   * @param {string} extension - раширение файлов
   * @param {string} path - путь где хранятся картинки для карт
   * @param {string} blockName - имя блока
   * @param {string} [height='300px'] - высота карты
   * @param {string} [shirt=null] - имя файла рубашки
   * @param {string} numColor - цвет лейбы с цифрой
   * @returns {Card[]}
   * @memberof MotherBlock02Component
   */

  protected createDeckCards(
    count: number,
    extension: string,
    path: string,
    blockName: string,
    height: string = '300px',
    shirt: string = null,
    numColor: string = null
  ): Card[] {
    const deck: Card[] = [];
    for (let i = 1; i <= count; i++) {
      if (this.excludeCards.includes(i)) {
        continue;
      }
      const tmpCard: Card = Object.assign({}, {
        name: `${i}.${extension}`,
        path: `${path}`,
        height: `${height}`,
        inverted: this.showFrontCard,
        moved: false,
        id: i - 1,
        block: `${blockName}`
      });
      if (shirt) {
        tmpCard.shirt = `${shirt}`;
      }
      if (numColor) {
        tmpCard.numColor = `${numColor}`;
      }
      deck.push(tmpCard);
    }
    return deck;
  }

  /**
   * отлавливаем когда карта возвращается с полотна в колоду
   *
   * @author A.Bondarenko
   * @date 2019-11-25
   * @param {Card} card
   * @memberof BaseCardBlock
   */
  public addToArray(card: Card) {
    if (card.hasOwnProperty('shirt')) {
      card.inverted = this.showFrontCard ? true : false;
    }
    this.deckCardsArray.splice(card.id, 0, card);
    this.deckCardsArray.sort((a, b) => a.id - b.id);
  }

  /**
   * двойной клик на карте в колоде
   * добавляем тип карты
   * убираем ее из колоды и отправляем на родителя
   *
   * @author A.Bondarenko
   * @date 2019-11-25
   * @param {Card} card
   * @memberof BaseCardBlock
   */
  public onDblclick(card: Card) {
    const index = this.deckCardsArray.indexOf(card);
    this.deckCardsArray[index].type = this.cardType;
    this.deckCardsArray.splice(index, 1);
    this.outCardToDesk.emit(card);
  }

  /**
   * перетосовать карты
   *
   * @author A.Bondarenko
   * @date 2019-11-25
   * @param {Card[]} array
   * @memberof BaseCardBlock
   */
  public resort(array: Card[] = this.deckCardsArray) {
    this._shuffleService.mixIt(array);
  }

  /**
   * перевернуть карты
   *
   * @author A.Bondarenko
   * @date 2019-11-27
   * @protected
   * @param {Card[]} array
   * @memberof BaseCardBlock
   */
  public inverte(array: Card[] = this.deckCardsArray) {
    this.showFrontCard = !this.showFrontCard;
    array.forEach((item) => {
      item.inverted = this.showFrontCard;
    });
  }

}
