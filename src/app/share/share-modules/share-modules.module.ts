import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SidebarModule } from 'primeng/sidebar';
import { CheckboxModule } from 'primeng/checkbox';
import { ToolbarModule } from 'primeng/toolbar';
import { AngularDraggableModule } from 'angular2-draggable';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    PanelModule,
    ButtonModule,
    InputTextareaModule,
    SidebarModule,
    CheckboxModule,
    ToolbarModule,
    AngularDraggableModule,
    TranslateModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    PanelModule,
    ButtonModule,
    InputTextareaModule,
    SidebarModule,
    CheckboxModule,
    ToolbarModule,
    AngularDraggableModule,
    TranslateModule,
    RouterModule
  ]
})
export class ShareModulesModule { }
