import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-skills-and-abilities-block-01',
  templateUrl: './skills-and-abilities-block-01.component.html',
  styleUrls: ['./skills-and-abilities-block-01.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class SkillsAndAbilitiesBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 128;
  public words = ['SKILLS_ABILITIES.TITLE'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['SKILLS_ABILITIES.TITLE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'skills-and-abilities',  'skillsAndAbilitiesBlock01Component', '300px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
