import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkillsAndAbilitiesPageComponent } from './skills-and-abilities-page.component'

const routes: Routes = [
  { path: 'skills-and-abilities', component: SkillsAndAbilitiesPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SkillsAndAbilitiesRoutingModule { }
