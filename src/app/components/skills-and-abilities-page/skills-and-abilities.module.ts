import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkillsAndAbilitiesPageComponent } from './skills-and-abilities-page.component';
import { SkillsAndAbilitiesBlock01Component } from './skills-and-abilities-block-01/skills-and-abilities-block-01.component';

import { SkillsAndAbilitiesRoutingModule } from './skills-and-abilities-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    SkillsAndAbilitiesPageComponent,
    SkillsAndAbilitiesBlock01Component
  ],
  imports: [
    CommonModule,
    SkillsAndAbilitiesRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule
  ],
  exports: [
    SkillsAndAbilitiesPageComponent,
    SkillsAndAbilitiesBlock01Component
  ]
})
export class SkillsAndAbilitiesModule { }
