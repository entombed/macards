import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubpersonalityPageComponent } from './subpersonality-page.component';
import { SubpersonalityBlock01Component } from './subpersonality-block-01/subpersonality-block-01.component';

import { SubpersonalityRoutingModule } from './subpersonality-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    SubpersonalityPageComponent,
    SubpersonalityBlock01Component,
  ],
  imports: [
    CommonModule,
    SubpersonalityRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    SubpersonalityPageComponent,
    SubpersonalityBlock01Component,
  ]
})
export class SubpersonalityModule { }
