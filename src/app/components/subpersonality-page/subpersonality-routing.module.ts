import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubpersonalityPageComponent } from './subpersonality-page.component';

const routes: Routes = [
  { path: 'subpersonality', component: SubpersonalityPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubpersonalityRoutingModule { }
