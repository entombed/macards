import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-subpersonality-block-01',
  templateUrl: './subpersonality-block-01.component.html',
  styleUrls: ['./subpersonality-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class SubpersonalityBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 73;
  public words = ['SUBPERSONALITY.TITLE'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['SUBPERSONALITY.TITLE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'subpersonality', 'subpersonalityBlock01Component', '300px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
