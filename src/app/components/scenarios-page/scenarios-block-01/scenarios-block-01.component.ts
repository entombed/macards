import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-scenarios-block-01',
  templateUrl: './scenarios-block-01.component.html',
  styleUrls: ['./scenarios-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})

export class ScenariosBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 50;
  public words = ['SCENARIOS.TITLE'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['SCENARIOS.TITLE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
     this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'png', 'scenarios', 'scenariosBlock01Component', '300px', 'shirt.png');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
