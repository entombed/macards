import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScenariosPageComponent } from './scenarios-page.component';
import { ScenariosBlock01Component } from './scenarios-block-01/scenarios-block-01.component';

import { ScenariosRoutingModule } from './scenarios-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    ScenariosPageComponent,
    ScenariosBlock01Component,
  ],
  imports: [
    CommonModule,
    ScenariosRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    ScenariosPageComponent,
    ScenariosBlock01Component,
  ]
})
export class ScenariosModule { }
