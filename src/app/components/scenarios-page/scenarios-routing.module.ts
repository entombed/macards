import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScenariosPageComponent } from './scenarios-page.component';

const routes: Routes = [
  { path: 'scenarios', component: ScenariosPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScenariosRoutingModule { }
