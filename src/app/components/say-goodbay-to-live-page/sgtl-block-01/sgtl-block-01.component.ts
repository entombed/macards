import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-sgtl-block-01',
  templateUrl: './sgtl-block-01.component.html',
  styleUrls: ['./sgtl-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class SgtlBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 64;
  public words = ['SGTL.TITLE', 'IMG'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['SGTL.TITLE']} - ${data['IMG']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'say-goodbye-to-live/img', 'sgtlBlock01Component', '300px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
