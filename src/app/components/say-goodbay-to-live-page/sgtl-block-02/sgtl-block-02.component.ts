import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-sgtl-block-02',
  templateUrl: './sgtl-block-02.component.html',
  styleUrls: ['./sgtl-block-02.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class SgtlBlock02Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 32;
  public words = ['SGTL.TITLE', 'TICKET'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['SGTL.TITLE']} - ${data['TICKET']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'say-goodbye-to-live/word',  'sgtlBlock02Component', '90px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
