import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SayGoodbayToLivePageComponent } from './say-goodbay-to-live-page.component';

const routes: Routes = [
  { path: 'say-goodbye-to-live', component: SayGoodbayToLivePageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SayGoodbayToLiveRoutingModule { }
