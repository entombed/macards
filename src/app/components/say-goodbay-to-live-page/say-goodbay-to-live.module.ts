import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SayGoodbayToLivePageComponent } from './say-goodbay-to-live-page.component';
import { SgtlBlock01Component } from './sgtl-block-01/sgtl-block-01.component';
import { SgtlBlock02Component } from './sgtl-block-02/sgtl-block-02.component';

import { SayGoodbayToLiveRoutingModule } from './say-goodbay-to-live-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    SayGoodbayToLivePageComponent,
    SgtlBlock01Component,
    SgtlBlock02Component,
  ],
  imports: [
    CommonModule,
    SayGoodbayToLiveRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    SayGoodbayToLivePageComponent,
    SgtlBlock01Component,
    SgtlBlock02Component,
  ]
})
export class SayGoodbayToLiveModule { }
