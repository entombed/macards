import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-cad-block-01',
  templateUrl: './cad-block-01.component.html',
  styleUrls: ['./cad-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class CadBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {
  // @Input() random;

  public maxCardCount = 21;
  public words = ['CAD.TITLE', 'CAD.GREEN'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['CAD.TITLE']} - ${data['CAD.GREEN']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'commands-and-decisions/green', 'cadBlock01Component', '300px', 'shirt.jpg', 'green');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }
}
