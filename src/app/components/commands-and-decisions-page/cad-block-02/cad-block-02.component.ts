import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-cad-block-02',
  templateUrl: './cad-block-02.component.html',
  styleUrls: ['./cad-block-02.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class CadBlock02Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public paramsButton = {
    resort: true,
    inverte: true
  };

  public maxCardCount = 26;
  public words = ['CAD.TITLE', 'CAD.PURPLE'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['CAD.TITLE']} - ${data['CAD.PURPLE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'commands-and-decisions/purple', 'cadBlock02Component', '300px', 'shirt.jpg', 'purple');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
