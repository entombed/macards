import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { CadBlock01Component } from '@components/commands-and-decisions-page/cad-block-01/cad-block-01.component';
import { CadBlock02Component } from '@components/commands-and-decisions-page/cad-block-02/cad-block-02.component';
import { CadBlock03Component } from '@components/commands-and-decisions-page/cad-block-03/cad-block-03.component';
import { WordsBlockComponent } from '@components/words-block/words-block.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { Route, ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';

@Component({
  selector: 'app-commands-and-decisions-page',
  templateUrl: './commands-and-decisions-page.component.html',
  styleUrls: ['./commands-and-decisions-page.component.css']
})
export class CommandsAndDecisionsPageComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(CadBlock01Component, { static: true }) private cadBlock01Component: CadBlock01Component;
  @ViewChild(CadBlock02Component, { static: true }) private cadBlock02Component: CadBlock02Component;
  @ViewChild(CadBlock03Component, { static: true }) private cadBlock03Component: CadBlock03Component;
  @ViewChild(WordsBlockComponent, { static: true }) private wordsBlockComponent: WordsBlockComponent;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }


  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }
}
