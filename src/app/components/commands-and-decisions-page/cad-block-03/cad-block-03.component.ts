import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-cad-block-03',
  templateUrl: './cad-block-03.component.html',
  styleUrls: ['./cad-block-03.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class CadBlock03Component extends BaseCardBlock implements OnInit, AfterContentInit {
  // @Input() random

  // public paramsButton = {
  //   resort: true,
  //   inverte: true
  // }

  public maxCardCount = 53;
  public words = ['CAD.TITLE', 'CAD.YELLOW'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['CAD.TITLE']} - ${data['CAD.YELLOW']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'commands-and-decisions/yellow', 'cadBlock03Component', '300px', 'shirt.jpg', 'yellow');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
