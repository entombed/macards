import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommandsAndDecisionsPageComponent } from './commands-and-decisions-page.component';

const routes: Routes = [
  { path: 'commands-and-decisions', component: CommandsAndDecisionsPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CommandsAndDecisionsRoutingModule { }
