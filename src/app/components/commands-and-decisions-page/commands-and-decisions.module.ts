import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommandsAndDecisionsPageComponent } from './commands-and-decisions-page.component';
import { CadBlock01Component } from './cad-block-01/cad-block-01.component';
import { CadBlock02Component } from './cad-block-02/cad-block-02.component';
import { CadBlock03Component } from './cad-block-03/cad-block-03.component';
import { CommandsAndDecisionsRoutingModule } from './commands-and-decisions-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';
import { DeskModule } from '@components/desk/desk.module';
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    CommandsAndDecisionsPageComponent,
    CadBlock01Component,
    CadBlock02Component,
    CadBlock03Component
  ],
  imports: [
    CommonModule,
    DeskModule,
    WordsBlockModule,
    ShareModulesModule,
    CommandsAndDecisionsRoutingModule
  ],
  exports: [
    CommandsAndDecisionsPageComponent,
    CadBlock01Component,
    CadBlock02Component,
    CadBlock03Component
  ]
})
export class CommandsAndDecisionsModule { }
