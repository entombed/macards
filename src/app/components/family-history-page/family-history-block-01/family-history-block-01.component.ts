import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-family-history-block-01',
  templateUrl: './family-history-block-01.component.html',
  styleUrls: ['./family-history-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class FamilyHistoryBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 54;
  public words = ['FAMILY_HISTORY.TITLE'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['FAMILY_HISTORY.TITLE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'family-history', 'familyHistoryBlock01Component', '224px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
