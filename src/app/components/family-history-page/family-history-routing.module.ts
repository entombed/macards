import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FamilyHistoryPageComponent } from './family-history-page.component';

const routes: Routes = [
  { path: 'family-history', component: FamilyHistoryPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FamilyHistoryRoutingModule { }
