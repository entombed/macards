import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FamilyHistoryPageComponent } from '@components/family-history-page/family-history-page.component';
import { FamilyHistoryBlock01Component } from '@components/family-history-page/family-history-block-01/family-history-block-01.component';

import { FamilyHistoryRoutingModule } from './family-history-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    FamilyHistoryPageComponent,
    FamilyHistoryBlock01Component,
  ],
  imports: [
    CommonModule,
    FamilyHistoryRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    FamilyHistoryPageComponent,
    FamilyHistoryBlock01Component,
  ]
})
export class FamilyHistoryModule { }
