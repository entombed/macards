import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OhPageComponent } from './oh-page.component';

const routes: Routes = [
  { path: 'oh', component: OhPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OhRoutingModule { }
