import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OhPageComponent } from './oh-page.component';
import { OhBlock01Component } from './oh-block-01/oh-block-01.component';
import { OhBlock02Component } from './oh-block-02/oh-block-02.component';

import { OhRoutingModule } from './oh-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    OhPageComponent,
    OhBlock01Component,
    OhBlock02Component,
  ],
  imports: [
    CommonModule,
    OhRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    OhPageComponent,
    OhBlock01Component,
    OhBlock02Component,
  ]
})
export class OhModule { }
