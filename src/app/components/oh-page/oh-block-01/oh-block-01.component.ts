import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';

import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-oh-block-01',
  templateUrl: './oh-block-01.component.html',
  styleUrls: ['./oh-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class OhBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 88;
  public excludeCards = [82, 83, 85];
  public words = ['OH.TITLE', 'IMG'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['OH.TITLE']} - ${data['IMG']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'oh/img', 'ohBlock01Component', '250px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }
}
