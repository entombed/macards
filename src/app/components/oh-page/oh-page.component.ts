import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { OhBlock01Component } from './oh-block-01/oh-block-01.component';
import { OhBlock02Component } from './oh-block-02/oh-block-02.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';

@Component({
  selector: 'app-oh-page',
  templateUrl: './oh-page.component.html',
  styleUrls: ['./oh-page.component.css']
})
export class OhPageComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(OhBlock01Component, { static: true }) private ohBlock01Component: OhBlock01Component;
  @ViewChild(OhBlock02Component, { static: true }) private ohBlock02Component: OhBlock02Component;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }



  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }
}
