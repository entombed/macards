import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';


@Component({
  selector: 'app-between-us-block-01',
  templateUrl: './between-us-block-01.component.html',
  styleUrls: ['./between-us-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class BetweenUsBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 46;
  public words = ['BETWEEN_US.TITLE'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
    .then((data) => {
      this.title = `${data['BETWEEN_US.TITLE']}`;
    })
    .catch((error) => {
      console.log(error);
    });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'between-us', 'betweenUsBlock01Component', '200px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }
}
