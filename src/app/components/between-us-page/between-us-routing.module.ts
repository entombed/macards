import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BetweenUsPageComponent } from './between-us-page.component';

const routes: Routes = [
  { path: 'between-us', component: BetweenUsPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BetweenUsRoutingModule { }
