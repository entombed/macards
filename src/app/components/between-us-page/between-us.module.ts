import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BetweenUsPageComponent } from './between-us-page.component';
import { BetweenUsBlock01Component } from './between-us-block-01/between-us-block-01.component';

import { BetweenUsRoutingModule } from './between-us-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    BetweenUsPageComponent,
    BetweenUsBlock01Component
  ],
  imports: [
    CommonModule,
    BetweenUsRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    BetweenUsPageComponent,
    BetweenUsBlock01Component
  ]
})
export class BetweenUsModule { }
