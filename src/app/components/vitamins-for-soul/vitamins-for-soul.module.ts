import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitaminsForSoulComponent } from './vitamins-for-soul.component';
import { VitaminsForSoulBlock01Component } from './vitamins-for-soul-block-01/vitamins-for-soul-block-01.component';
import { VitaminsForSoulRoutingModule } from '@components/vitamins-for-soul/vitamins-for-soul-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';
import { DeskModule } from '@components/desk/desk.module';
import { WordsBlockModule } from '@components/words-block/words-block.module';


@NgModule({
  declarations: [
    VitaminsForSoulComponent,
    VitaminsForSoulBlock01Component
  ],
  imports: [
    CommonModule,
    VitaminsForSoulRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    VitaminsForSoulComponent,
    VitaminsForSoulBlock01Component
  ]
})
export class VitaminsForSoulModule { }
