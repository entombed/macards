import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VitaminsForSoulComponent } from '@components/vitamins-for-soul/vitamins-for-soul.component';

const routes: Routes = [
  { path: 'vitamins-for-soul', component: VitaminsForSoulComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VitaminsForSoulRoutingModule { }
