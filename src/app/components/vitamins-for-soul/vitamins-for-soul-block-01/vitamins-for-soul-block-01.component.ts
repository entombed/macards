import { AfterContentInit, Component, OnInit } from '@angular/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';
import { ShuffleService } from '@services/shuffle.service';
import { TranslateService } from '@ngx-translate/core';
import { AnimationInDeck } from '@share/animation';

@Component({
  selector: 'app-vitamins-for-soul-block-01',
  templateUrl: './vitamins-for-soul-block-01.component.html',
  styleUrls: ['./vitamins-for-soul-block-01.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class VitaminsForSoulBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 80;
  public words = ['VITAMINS_FOR_SOUL.TITLE', 'IMG'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(
      _shuffleService,
      translate
    );
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['VITAMINS_FOR_SOUL.TITLE']} - ${data['IMG']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit(): void {
    this.deckCardsArray = this.createDeckCards(
      this.maxCardCount,
      'jpg',
      'vitamins-for-soul',
      'vitaminsForSoulBlock01Component',
      '300px',
      'shirt.jpg'
    );
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
