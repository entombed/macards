import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ResizeAreaMenuComponent } from '@components/desk/resize-area-menu/resize-area-menu.component';
import { Card } from '@share/card';
import { AnimationOnDesk } from '@share/animation';

@Component({
  selector: 'app-desk',
  templateUrl: './desk.component.html',
  styleUrls: ['./desk.component.css'],
  animations: [
    AnimationOnDesk
  ]
})

export class DeskComponent implements OnInit {

  constructor() { }

  @ViewChild(ResizeAreaMenuComponent, { static: true }) public resizeAreaMenuComponent: ResizeAreaMenuComponent;

  @Output() outElementBack = new EventEmitter<any>();
  public elongatedCardsArray: any = [];
  public elongatedWordsArray: any = [];
  public inputTextArray: any = [];
  public zIndex: number = null;
  public areaProperties = {
    height: '',
    width: '',
    step: 400
  };
  // public defaultHeightImage = '300px';
  public displayCardNumber = true;

  ngOnInit() {
  }

  public addToArray(data: Card) {
    data.moved = true;
    if (data.type === 'image') {
      this.elongatedCardsArray.push(data);
    } else if (data.type === 'word') {
      this.elongatedWordsArray.push(data);
    } else if (data.type === 'text') {
      this.inputTextArray.push(data);
    }
  }

  public onClick(event: MouseEvent, data: Card) {
    let array = [];
    if (data.type === 'image') {
      array = this.elongatedCardsArray;
    } else if (data.type === 'word') {
      array = this.elongatedWordsArray;
    } else if (data.type === 'text') {
      array = this.inputTextArray;
    }

    if (event.ctrlKey === true) {
      data.moved = false;
      delete data.type;
      delete data.position;
      const index = array.indexOf(data);
      array.splice(index, 1);
      this.outElementBack.emit(data);
    } else if (event.altKey === true && data.type === 'image' && 'shirt' in data) {
      data.inverted = !data.inverted;
    }
  }

  public showResizeMenu() {
    this.resizeAreaMenuComponent.show();
  }

  public pushShowNumber(status: boolean) {
    this.displayCardNumber = status;
  }

  public onStart(event, data: Card) {
    this.setZIndex(event);
  }

  public onMoving(data: Card) {
    if (data.type === 'text') {
      data.locked = true;
    }
  }

  public onMoveEnd(data: Card) {
    if (data.type === 'text') {
      data.locked = false;
    }
  }

  public setZIndex(target) {
    if (this.zIndex !== target.style.zIndex) {
      this.zIndex = this.zIndex + 1;
      target.style.zIndex = this.zIndex;
    }
  }

  public setPosition(data: Card) {
    data.position = { x: 0, y: 0 };
  }
}
