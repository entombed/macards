import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { KeyCode } from '@share/card';

@Component({
  selector: 'app-deck-menu',
  templateUrl: './deck-menu.component.html',
  styleUrls: ['./deck-menu.component.css']
})
export class DeckMenuComponent implements OnInit {

  // KEY_CODE = {
  //   ESC: 'Escape',
  //   ENTER: 13
  // };
  public keyCode: KeyCode;
  public showMenu = false;
  public currentUrl = '';

  public menu = [
    {
      url: '/scenarios',
      label: 'SCENARIOS.TITLE',
      random: true
    },
    {
      url: '/robots',
      label: 'ROBOTS.TITLE',
      random: false
    },
    {
      url: '/commands-and-decisions',
      label: 'CAD.TITLE',
      random: true
    },
    {
      url: '/about-me',
      label: 'ABOUT_ME.TITLE',
      random: false
    },
    {
      url: '/oh',
      label: 'OH.TITLE',
      random: true
    },
    {
      url: '/merlin',
      label: 'MERLIN.TITLE',
      random: true
    },
    {
      url: '/about-you',
      label: 'ABOUT_YOU.TITLE',
      random: true
    },
    {
      url: '/umbrellas',
      label: 'UMBRELLAS.TITLE',
      random: true
    },
    {
      url: '/say-goodbye-to-live',
      label: 'SGTL.TITLE',
      random: true
    },
    {
      url: '/prism',
      label: 'PRISM.TITLE',
      random: true
    },
    {
      url: '/river',
      label: 'RIVER.TITLE',
      random: true
    },
    {
      url: '/between-us',
      label: 'BETWEEN_US.TITLE',
      random: true
    },
    {
      url: '/family-history',
      label: 'FAMILY_HISTORY.TITLE',
      random: true
    },
    {
      url: '/subpersonality',
      label: 'SUBPERSONALITY.TITLE',
      random: true
    },
    {
      url: '/mixed-feelings',
      label: 'MIXED_FEELINGS.TITLE',
      random: true
    },
    {
      url: '/mother',
      label: 'MOTHER.TITLE',
      random: true
    },
    {
      url: '/daughter-of-father',
      label: 'FATHER.TITLE',
      random: true
    },
    {
      url: '/life-scripts',
      label: 'LIFE_SCRIPTS.TITLE',
      random: true
    },
    {
      url: '/skills-and-abilities',
      label: 'SKILLS_ABILITIES.TITLE',
      random: true
    },
    {
      url: '/cockroaches',
      label: 'COCKROACHES.TITLE',
      random: true
    },
    {
      url: '/duet',
      label: 'DUET.TITLE',
      random: true
    },
    {
      url: '/roles-and-archetypes',
      label: 'ROLES_AND_ARCHETYPES.TITLE',
      random: true
    },
    {
      url: '/fears-benefits',
      label: 'FEARS_BENEFITS.TITLE',
      random: true
    },
    {
      url: '/complexes-psy-defense',
      label: 'COMPLEXES_PSY_DEFENSE.TITLE',
      random: true
    },
    {
      url: '/vitamins-for-soul',
      label: 'VITAMINS_FOR_SOUL.TITLE',
      random: true
    },
    {
      url: '/custom-page',
      label: 'CUSTOM_PAGE.TITLE',
      random: false
    }
  ];

  constructor(
    private _router: Router,
    private translate: TranslateService
  ) {
    translate.setDefaultLang('ru');
    translate.use('ru');
    this.keyCode = new KeyCode();
  }

  ngOnInit() {
  }

  @HostListener('window:keyup', ['$event'])
  keyUpEvent(event: KeyboardEvent) {
    if (event.code === this.keyCode.esc) {
      this.showMenu = false;
    }
  }

  public show() {
    this.showMenu = true;
    const urlArray = this._router.url.toString().split('?');
    this.currentUrl = urlArray[0];
  }

}
