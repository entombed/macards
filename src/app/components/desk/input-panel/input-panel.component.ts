import { Component, OnInit, EventEmitter, Output, ElementRef, ViewChild, HostListener, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { KeyCode } from '@share/card';

@Component({
  selector: 'app-input-panel',
  templateUrl: './input-panel.component.html',
  styleUrls: ['./input-panel.component.css']
})
export class InputPanelComponent implements OnInit {

  public showPanel = false;
  public inputText = '';
  public buttonText = 'BUTTONS.ADD';
  // public KEY_CODE = {
  //   ESC: 'Escape',
  //   ENTER: 'Enter'
  // };
  public keyCode: KeyCode;

  @Output() outTextToDesk = new EventEmitter<any>();
  @ViewChild('inputRef', { static: true }) inputField: ElementRef;

  constructor(
    private translate: TranslateService,
    private renderer: Renderer2
  ) {
    translate.setDefaultLang('ru');
    translate.use('ru');
    this.keyCode = new KeyCode();
  }

  ngOnInit() {
  }

  @HostListener('window:keyup', ['$event'])
  keyUpEvent(event: KeyboardEvent) {
    if (event.code === this.keyCode.esc) {
      this.showPanel = false;
    }
  }

  public focus() {
    const element = this.inputField.nativeElement;
    this.renderer.selectRootElement(element).focus();
  }

  public show() {
    this.showPanel = true;
  }

  public getText() {
    const text = {
      name: this.inputField.nativeElement.value,
      type: 'text',
      moved: false,
      locked: false,
      block: 'app-input-panel'
    };
    this.outTextToDesk.emit(text);
    this.inputField.nativeElement.value = null;
    this.focus();
  }

}
