import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { KeyCode } from '@share/card';

@Component({
  selector: 'app-resize-area-menu',
  templateUrl: './resize-area-menu.component.html',
  styleUrls: ['./resize-area-menu.component.css']
})
export class ResizeAreaMenuComponent implements OnInit {

  @Input() inAreaProperties;
  @Output() outShowNumber = new EventEmitter<any>();
  public display = false;
  public displayNumber = true;
  public labelNum = 'BUTTONS.HIDE_NUM';
  // public KEY_CODE = {
  //   ESC: 'Escape',
  //   ENTER: 'Enter'
  // };
  public keyCode: KeyCode;
  constructor(
    private translate: TranslateService
  ) {
    translate.setDefaultLang('ru');
    translate.use('ru');
    this.keyCode = new KeyCode();
  }

  @HostListener('window:keyup', ['$event'])
  keyUpEvent(event: KeyboardEvent) {
    if (event.code === this.keyCode.esc) {
      this.display = false;
    }
  }

  ngOnInit() {
  }

  public show() {
    this.display = true;
  }

  public showNumber() {
    this.displayNumber = !this.displayNumber;
    this.labelNum = this.displayNumber ? 'BUTTONS.HIDE_NUM' : 'BUTTONS.SHOW_NUM';
    this.outShowNumber.emit(this.displayNumber);
  }

  public checkSizeArea() {
    let mainArea;
    mainArea = document.querySelector('.cardDeskBlock').getBoundingClientRect();
    this.inAreaProperties.height = mainArea.height;
    this.inAreaProperties.width = mainArea.width;
  }

  public reset(data) {
    switch (data) {
      case 'width':
        this.inAreaProperties.width = '';
        break;
      case 'height':
        this.inAreaProperties.height = '';
        break;
    }
  }

  public changeWidth(action) {
    this.checkSizeArea();
    this.inAreaProperties.width = action === 'increase'
      ? this.inAreaProperties.width + this.inAreaProperties.step + 'px'
      : this.inAreaProperties.width - this.inAreaProperties.step + 'px';
  }

  public changeHeight(action) {
    this.checkSizeArea();
    this.inAreaProperties.height = action === 'increase'
      ? this.inAreaProperties.height + this.inAreaProperties.step + 'px'
      : this.inAreaProperties.height - this.inAreaProperties.step + 'px';
  }

}
