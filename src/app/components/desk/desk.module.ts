import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskComponent } from './desk.component';
import { InputPanelComponent } from './input-panel/input-panel.component';
import { DeckMenuComponent } from './deck-menu/deck-menu.component';
import { ResizeAreaMenuComponent } from './resize-area-menu/resize-area-menu.component';
import { AutoFocusDirective } from '@directives/auto-focus.directive';

@NgModule({
  declarations: [
    DeskComponent,
    InputPanelComponent,
    DeckMenuComponent,
    ResizeAreaMenuComponent,
    AutoFocusDirective
  ],
  imports: [
    CommonModule,
    ShareModulesModule
  ],
  exports: [
    DeskComponent,
    InputPanelComponent,
    DeckMenuComponent,
    ResizeAreaMenuComponent
  ]
})

export class DeskModule { }
