import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-mixed-feelings-block-01',
  templateUrl: './mixed-feelings-block-01.component.html',
  styleUrls: ['./mixed-feelings-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class MixedFeelingsBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 60;
  public words = ['MIXED_FEELINGS.TITLE'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['MIXED_FEELINGS.TITLE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'mixed-feelings', 'mixedFeelingsBlock01Component', '300px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
