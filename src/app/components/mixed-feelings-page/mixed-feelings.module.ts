import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MixedFeelingsPageComponent } from './mixed-feelings-page.component';
import { MixedFeelingsBlock01Component } from './mixed-feelings-block-01/mixed-feelings-block-01.component';

import { MixedFeelingsRoutingModule } from './mixed-feelings-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    MixedFeelingsPageComponent,
    MixedFeelingsBlock01Component,
  ],
  imports: [
    CommonModule,
    MixedFeelingsRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    MixedFeelingsPageComponent,
    MixedFeelingsBlock01Component,
  ]
})
export class MixedFeelingsModule { }
