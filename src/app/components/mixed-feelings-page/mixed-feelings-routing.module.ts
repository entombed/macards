import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MixedFeelingsPageComponent } from './mixed-feelings-page.component';

const routes: Routes = [
  { path: 'mixed-feelings', component: MixedFeelingsPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MixedFeelingsRoutingModule { }
