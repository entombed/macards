import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LifeScriptsRoutingModule } from './life-scripts-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

import { LifeScriptsPageComponent } from './life-scripts-page.component';
import { LifeScriptsBlock01Component } from './life-scripts-block-01/life-scripts-block-01.component';
import { LifeScriptsBlock02Component } from './life-scripts-block-02/life-scripts-block-02.component';

@NgModule({
  declarations: [
    LifeScriptsPageComponent,
    LifeScriptsBlock01Component,
    LifeScriptsBlock02Component,
  ],
  imports: [
    CommonModule,
    LifeScriptsRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    LifeScriptsPageComponent,
    LifeScriptsBlock01Component,
    LifeScriptsBlock02Component,
  ]
})
export class LifeScriptsModule { }
