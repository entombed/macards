import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { LifeScriptsBlock01Component } from './life-scripts-block-01/life-scripts-block-01.component';
import { LifeScriptsBlock02Component } from './life-scripts-block-02/life-scripts-block-02.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';

@Component({
  selector: 'app-life-scripts-page',
  templateUrl: './life-scripts-page.component.html',
  styleUrls: ['./life-scripts-page.component.css']
})
export class LifeScriptsPageComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(LifeScriptsBlock01Component, { static: true }) private lifeScriptsBlock01Component: LifeScriptsBlock01Component;
  @ViewChild(LifeScriptsBlock02Component, { static: true }) private lifeScriptsBlock02Component: LifeScriptsBlock02Component;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }

  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

}
