import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LifeScriptsPageComponent } from './life-scripts-page.component';

const routes: Routes = [
  { path: 'life-scripts', component: LifeScriptsPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeScriptsRoutingModule { }
