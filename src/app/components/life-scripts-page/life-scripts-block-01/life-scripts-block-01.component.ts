import { Component, OnInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-life-scripts-block-01',
  templateUrl: './life-scripts-block-01.component.html',
  styleUrls: ['./life-scripts-block-01.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class LifeScriptsBlock01Component extends BaseCardBlock implements OnInit {

  public maxCardCount = 64;
  public words = ['LIFE_SCRIPTS.RED', 'LIFE_SCRIPTS.TITLE'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['LIFE_SCRIPTS.RED']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'life-script/red', 'lifeScriptsBlock01Component', '300px', 'shirt.jpg');
    super.ngOnInit();
  }

}
