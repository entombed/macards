import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-umbrellas-block-01',
  templateUrl: './umbrellas-block-01.component.html',
  styleUrls: ['./umbrellas-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class UmbrellasBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 64;
  public words = ['UMBRELLAS.TITLE', 'IMG'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['UMBRELLAS.TITLE']} - ${data['IMG']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(
      this.maxCardCount,
      'jpg',
      'umbrellas/img',
      'umbrellasBlock01Component',
      '300px',
      'shirt.jpg'
    );
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
