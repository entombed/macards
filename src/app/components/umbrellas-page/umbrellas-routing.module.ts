import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UmbrellasPageComponent } from './umbrellas-page.component';

const routes: Routes = [
  { path: 'umbrellas', component: UmbrellasPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UmbrellasRoutingModule { }
