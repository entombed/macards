import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UmbrellasPageComponent } from './umbrellas-page.component';
import { UmbrellasBlock01Component } from './umbrellas-block-01/umbrellas-block-01.component';
import { UmbrellasBlock02Component } from './umbrellas-block-02/umbrellas-block-02.component';

import { UmbrellasRoutingModule } from './umbrellas-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    UmbrellasPageComponent,
    UmbrellasBlock01Component,
    UmbrellasBlock02Component,
  ],
  imports: [
    CommonModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
    UmbrellasRoutingModule,
  ],
  exports: [
    UmbrellasPageComponent,
    UmbrellasBlock01Component,
    UmbrellasBlock02Component,
  ]
})
export class UmbrellasModule { }
