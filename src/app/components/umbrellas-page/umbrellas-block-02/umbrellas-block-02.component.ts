import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-umbrellas-block-02',
  templateUrl: './umbrellas-block-02.component.html',
  styleUrls: ['./umbrellas-block-02.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class UmbrellasBlock02Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 64;
  public words = ['UMBRELLAS.TITLE', 'TICKET'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['UMBRELLAS.TITLE']} - ${data['TICKET']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(
      this.maxCardCount,
      'jpg',
      'umbrellas/word',
      'umbrellasBlock02Component',
      '90px',
      'shirt.jpg'
    );
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
