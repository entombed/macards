import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DuetPageComponent } from './duet-page.component';


const routes: Routes = [
  { path: 'duet', component: DuetPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DuetRoutingModule { }
