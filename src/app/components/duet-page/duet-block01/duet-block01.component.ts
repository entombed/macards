import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-duet-block01',
  templateUrl: './duet-block01.component.html',
  styleUrls: ['./duet-block01.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class DuetBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 96;
  public words = ['DUET.TITLE'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['DUET.TITLE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'duet', 'duetBlock01Component', '300px', 'shirt.jpg');
    super.ngOnInit();

  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
