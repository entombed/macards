import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';
import { DeskModule } from '@components/desk/desk.module';

import { DuetPageComponent } from './duet-page.component';
import { DuetBlock01Component } from './duet-block01/duet-block01.component';
import { DuetRoutingModule } from './duet-routing.module';


@NgModule({
  declarations: [
    DuetPageComponent,
    DuetBlock01Component
  ],
  imports: [
    CommonModule,
    DuetRoutingModule,
    ShareModulesModule,
    DeskModule
  ],
  exports: [
    DuetPageComponent,
    DuetBlock01Component
  ]
})
export class DuetModule { }
