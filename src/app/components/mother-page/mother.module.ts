import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MotherPageComponent } from './mother-page.component';
import { MotherBlock01Component } from './mother-block-01/mother-block-01.component';
import { MotherBlock02Component } from './mother-block-02/mother-block-02.component';

import { MotherRoutingModule } from './mother-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';
import { DeskModule } from '@components/desk/desk.module';
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    MotherPageComponent,
    MotherBlock01Component,
    MotherBlock02Component
  ],
  imports: [
    CommonModule,
    MotherRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule
  ],
  exports: [
    MotherBlock01Component,
    MotherBlock02Component
  ]
})
export class MotherModule { }
