import { Component, OnInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-mother-block-01',
  templateUrl: './mother-block-01.component.html',
  styleUrls: ['./mother-block-01.component.css'],
  animations: [
    AnimationInDeck
  ]
})

export class MotherBlock01Component extends BaseCardBlock implements OnInit {
  public maxCardCount = 114;
  public words = ['MOTHER.TITLE', 'MOTHER.SITUATIONAL'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
    .then((data) => {
      this.title = `${data['MOTHER.TITLE']} - ${data['MOTHER.SITUATIONAL']}`;
    })
    .catch((error) => {
      console.log(error);
    });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'png', 'mother/situational', 'motherBlock01Component', '350px', 'shirt.png');
    super.ngOnInit();
  }
}
