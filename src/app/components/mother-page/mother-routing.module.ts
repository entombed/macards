import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MotherPageComponent } from './mother-page.component';

const routes: Routes = [
  { path: 'mother', component: MotherPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotherRoutingModule { }
