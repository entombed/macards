import { Component, OnInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-mother-block-02',
  templateUrl: './mother-block-02.component.html',
  styleUrls: ['./mother-block-02.component.css'],
  animations: [
    AnimationInDeck
  ]
})

export class MotherBlock02Component extends BaseCardBlock implements OnInit {
  public maxCardCount = 34;
  public words = ['MOTHER.TITLE', 'MOTHER.ANIMALS'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
    .then((data) => {
      this.title = `${data['MOTHER.TITLE']} - ${data['MOTHER.ANIMALS']}`;
    })
    .catch((error) => {
      console.log(error);
    });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'png', 'mother/animals', 'motherBlock02Component', '350px', 'shirt.png');
    super.ngOnInit();
  }

}
