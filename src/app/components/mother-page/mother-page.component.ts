import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { MotherBlock01Component } from './mother-block-01/mother-block-01.component';
import { MotherBlock02Component } from './mother-block-02/mother-block-02.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';

@Component({
  selector: 'app-mother-page',
  templateUrl: './mother-page.component.html',
  styleUrls: ['./mother-page.component.css']
})
export class MotherPageComponent implements OnInit {
  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(MotherBlock01Component, { static: true }) private motherBlock01Component: MotherBlock01Component;
  @ViewChild(MotherBlock02Component, { static: true }) private motherBlock02Component: MotherBlock02Component;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }

  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

}
