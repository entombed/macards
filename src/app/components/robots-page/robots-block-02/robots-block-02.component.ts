import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-robots-block-02',
  templateUrl: './robots-block-02.component.html',
  styleUrls: ['./robots-block-02.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class RobotsBlock02Component extends BaseCardBlock implements OnInit {

  public maxCardCount = 22;
  public words = ['ROBOTS.TITLE', 'BODY'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['ROBOTS.TITLE']} - ${data['BODY']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'robots/body', 'robotsBlock02Component', '200px');
    super.ngOnInit();
  }

}
