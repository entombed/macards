import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-robots-block-03',
  templateUrl: './robots-block-03.component.html',
  styleUrls: ['./robots-block-03.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class RobotsBlock03Component extends BaseCardBlock implements OnInit {

  public maxCardCount = 28;
  public words = ['ROBOTS.TITLE', 'FOOT'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['ROBOTS.TITLE']} - ${data['FOOT']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'robots/foot', 'robotsBlock03Component', '200px');
    super.ngOnInit();
  }

}
