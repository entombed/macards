import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RobotsPageComponent } from '@components/robots-page/robots-page.component';
import { RobotsBlock01Component } from '@components/robots-page/robots-block-01/robots-block-01.component';
import { RobotsBlock02Component } from '@components/robots-page/robots-block-02/robots-block-02.component';
import { RobotsBlock03Component } from '@components/robots-page/robots-block-03/robots-block-03.component';

import { RobotsRoutingModule } from './robots-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    RobotsPageComponent,
    RobotsBlock01Component,
    RobotsBlock02Component,
    RobotsBlock03Component,
  ],
  imports: [
    CommonModule,
    RobotsRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
      ],
  exports: [
    RobotsPageComponent,
    RobotsBlock01Component,
    RobotsBlock02Component,
    RobotsBlock03Component,
  ]
})
export class RobotsModule { }
