import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RobotsPageComponent } from './robots-page.component';

const routes: Routes = [
  { path: 'robots', component: RobotsPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RobotsRoutingModule { }
