import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { RobotsBlock01Component } from '@components/robots-page/robots-block-01/robots-block-01.component';
import { RobotsBlock02Component } from '@components/robots-page/robots-block-02/robots-block-02.component';
import { RobotsBlock03Component } from '@components/robots-page/robots-block-03/robots-block-03.component';
import { WordsBlockComponent } from '@components/words-block/words-block.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';

@Component({
  selector: 'app-robots-page',
  templateUrl: './robots-page.component.html',
  styleUrls: ['./robots-page.component.css']
})
export class RobotsPageComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(RobotsBlock01Component, { static: true }) private robotsBlock01Component: RobotsBlock01Component;
  @ViewChild(RobotsBlock02Component, { static: true }) private robotsBlock02Component: RobotsBlock02Component;
  @ViewChild(RobotsBlock03Component, { static: true }) private robotsBlock03Component: RobotsBlock03Component;
  @ViewChild(WordsBlockComponent, { static: true }) private wordsBlockComponent: WordsBlockComponent;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }


  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

}
