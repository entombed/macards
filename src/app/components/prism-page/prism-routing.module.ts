import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrismPageComponent } from './prism-page.component';

const routes: Routes = [
  { path: 'prism', component: PrismPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrismRoutingModule { }
