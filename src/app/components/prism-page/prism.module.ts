import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrismPageComponent } from './prism-page.component';
import { PrismBlock01Component } from './prism-block-01/prism-block-01.component';

import { PrismRoutingModule } from './prism-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    PrismPageComponent,
    PrismBlock01Component,
  ],
  imports: [
    CommonModule,
    PrismRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    PrismPageComponent,
    PrismBlock01Component,
  ]
})
export class PrismModule { }
