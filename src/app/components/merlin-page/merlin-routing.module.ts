import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MerlinPageComponent } from './merlin-page.component';

const routes: Routes = [
  { path: 'merlin', component: MerlinPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerlinRoutingModule { }
