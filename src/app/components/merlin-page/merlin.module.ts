import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MerlinPageComponent } from './merlin-page.component';
import { MerlinBlock01Component } from './merlin-block-01/merlin-block-01.component';

import { MerlinRoutingModule } from './merlin-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    MerlinPageComponent,
    MerlinBlock01Component,
  ],
  imports: [
    CommonModule,
    MerlinRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    MerlinPageComponent,
    MerlinBlock01Component,
  ]
})
export class MerlinModule { }
