import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplexesPsyDefenseComponent } from './complexes-psy-defense.component';


const routes: Routes = [
  { path: 'complexes-psy-defense', component: ComplexesPsyDefenseComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplexesPsyDefenseRoutingModule { }
