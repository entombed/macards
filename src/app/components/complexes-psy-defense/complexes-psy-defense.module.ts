import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';
import { DeskModule } from '@components/desk/desk.module';

import { ComplexesPsyDefenseComponent } from './complexes-psy-defense.component';
import { ComplexesPsyDefenseBlock01Component } from './complexes-psy-defense-block01/complexes-psy-defense-block01.component';
import { ComplexesPsyDefenseBlock02Component } from './complexes-psy-defense-block02/complexes-psy-defense-block02.component';
import { ComplexesPsyDefenseRoutingModule } from './complexes-psy-defense-routing.module';


@NgModule({
  declarations: [
    ComplexesPsyDefenseComponent,
    ComplexesPsyDefenseBlock01Component,
    ComplexesPsyDefenseBlock02Component
  ],
  imports: [
    CommonModule,
    ComplexesPsyDefenseRoutingModule,
    ShareModulesModule,
    DeskModule
  ],
  exports: [
    ComplexesPsyDefenseComponent,
    ComplexesPsyDefenseBlock01Component,
    ComplexesPsyDefenseBlock02Component
  ]
})
export class ComplexesPsyDefenseModule { }
