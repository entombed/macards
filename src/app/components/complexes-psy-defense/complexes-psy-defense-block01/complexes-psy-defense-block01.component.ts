import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-complexes-psy-defense-block01',
  templateUrl: './complexes-psy-defense-block01.component.html',
  styleUrls: ['./complexes-psy-defense-block01.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class ComplexesPsyDefenseBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 64;
  public words = ['COMPLEXES_PSY_DEFENSE.COMPLEXES'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['COMPLEXES_PSY_DEFENSE.COMPLEXES']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(
      this.maxCardCount,
      'jpg',
      'complexes-psy-defense/complexes',
      'complexesPsyDefenseBlock01Component',
      '300px',
      'shirt.jpg'
    );
    super.ngOnInit();

  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }


}
