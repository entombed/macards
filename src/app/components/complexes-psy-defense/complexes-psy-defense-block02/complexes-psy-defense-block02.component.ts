import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-complexes-psy-defense-block02',
  templateUrl: './complexes-psy-defense-block02.component.html',
  styleUrls: ['./complexes-psy-defense-block02.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class ComplexesPsyDefenseBlock02Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 64;
  public words = ['COMPLEXES_PSY_DEFENSE.DEFENSE'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['COMPLEXES_PSY_DEFENSE.DEFENSE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(
      this.maxCardCount,
      'jpg',
      'complexes-psy-defense/psy-defense',
      'complexesPsyDefenseBlock02Component',
      '300px',
      'shirt.jpg'
    );
    super.ngOnInit();

  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }


}
