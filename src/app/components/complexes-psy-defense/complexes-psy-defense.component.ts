import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';
import { ComplexesPsyDefenseBlock02Component } from './complexes-psy-defense-block02/complexes-psy-defense-block02.component';
import { ComplexesPsyDefenseBlock01Component } from './complexes-psy-defense-block01/complexes-psy-defense-block01.component';

@Component({
  selector: 'app-complexes-psy-defense',
  templateUrl: './complexes-psy-defense.component.html',
  styleUrls: ['./complexes-psy-defense.component.css']
})
export class ComplexesPsyDefenseComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(
    ComplexesPsyDefenseBlock01Component, { static: true }
  ) private complexesPsyDefenseBlock01Component: ComplexesPsyDefenseBlock01Component;
  @ViewChild(
    ComplexesPsyDefenseBlock02Component, { static: true }
  ) private complexesPsyDefenseBlock02Component: ComplexesPsyDefenseBlock02Component;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }


  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

}
