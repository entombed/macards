import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RiverPageComponent } from './river-page.component';

const routes: Routes = [
  { path: 'river', component: RiverPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RiverRoutingModule { }
