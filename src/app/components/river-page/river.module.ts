import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RiverPageComponent } from './river-page.component';
import { RiverBlock01Component } from './river-block-01/river-block-01.component';

import { RiverRoutingModule } from './river-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    RiverPageComponent,
    RiverBlock01Component,
  ],
  imports: [
    CommonModule,
    RiverRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule,
  ],
  exports: [
    RiverPageComponent,
    RiverBlock01Component,
  ]
})
export class RiverModule { }
