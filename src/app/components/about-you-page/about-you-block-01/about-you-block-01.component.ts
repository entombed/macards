import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-about-you-block-01',
  templateUrl: './about-you-block-01.component.html',
  styleUrls: ['./about-you-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})
export class AboutYouBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 64;
  public words = ['ABOUT_YOU.TITLE'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['ABOUT_YOU.TITLE']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'about-you', 'aboutYouBlock01Component', '300px', 'shirt.jpg');
    super.ngOnInit();

  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
