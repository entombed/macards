import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutYouPageComponent } from './about-you-page.component';

const routes: Routes = [
  { path: 'about-you', component: AboutYouPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutYouRoutingModule { }
