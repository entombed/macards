import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutYouPageComponent } from './about-you-page.component';
import { AboutYouBlock01Component } from './about-you-block-01/about-you-block-01.component';

import { AboutYouRoutingModule } from './about-you-routing.module';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';
import { DeskModule } from '@components/desk/desk.module';
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    AboutYouPageComponent,
    AboutYouBlock01Component
  ],
  imports: [
    CommonModule,
    AboutYouRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule
  ],
  exports: [
    AboutYouPageComponent,
    AboutYouBlock01Component
  ]
})
export class AboutYouModule { }
