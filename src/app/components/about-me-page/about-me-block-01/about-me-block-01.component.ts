import { Component, OnInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-about-me-block-01',
  templateUrl: './about-me-block-01.component.html',
  styleUrls: ['./about-me-block-01.component.css'],

  animations: [
    AnimationInDeck
  ]
})

export class AboutMeBlock01Component extends BaseCardBlock implements OnInit {
  public maxCardCount = 30;
  public words = ['ABOUT_ME.TITLE', 'HEAD'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
    .then((data) => {
      this.title = `${data['ABOUT_ME.TITLE']} - ${data['HEAD']}`;
    })
    .catch((error) => {
      console.log(error);
    });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'about-me/head', 'aboutMeBlock01Component', '200px');
    super.ngOnInit();
  }
}
