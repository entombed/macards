import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutMePageComponent } from '@components/about-me-page/about-me-page.component';
import { AboutMeBlock01Component } from './about-me-block-01/about-me-block-01.component';
import { AboutMeBlock02Component } from './about-me-block-02/about-me-block-02.component';
import { AboutMeBlock03Component } from './about-me-block-03/about-me-block-03.component';
import { AboutMeRoutingModule } from './about-me-routing.module';

import { ShareModulesModule } from '@share/share-modules/share-modules.module'
import { DeskModule } from '@components/desk/desk.module'
import { WordsBlockModule } from '@components/words-block/words-block.module';

@NgModule({
  declarations: [
    AboutMePageComponent,
    AboutMeBlock01Component,
    AboutMeBlock02Component,
    AboutMeBlock03Component
  ],
  imports: [
    CommonModule,
    AboutMeRoutingModule,
    ShareModulesModule,
    DeskModule,
    WordsBlockModule
  ],
  exports: [
    AboutMePageComponent,
    AboutMeBlock01Component,
    AboutMeBlock02Component,
    AboutMeBlock03Component
  ]
})
export class AboutMeModule { }
