import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { AboutMeBlock01Component } from './about-me-block-01/about-me-block-01.component';
import { AboutMeBlock02Component } from './about-me-block-02/about-me-block-02.component';
import { AboutMeBlock03Component } from './about-me-block-03/about-me-block-03.component';
import { WordsBlockComponent } from '@components/words-block/words-block.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Card } from '@share/card';

@Component({
  selector: 'app-about-me-page',
  templateUrl: './about-me-page.component.html',
  styleUrls: ['./about-me-page.component.css']
})
export class AboutMePageComponent implements OnInit, OnDestroy {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(AboutMeBlock01Component, { static: true }) private aboutMeBlock01Component: AboutMeBlock01Component;
  @ViewChild(AboutMeBlock02Component, { static: true }) private aboutMeBlock02Component: AboutMeBlock02Component;
  @ViewChild(AboutMeBlock03Component, { static: true }) private aboutMeBlock03Component: AboutMeBlock03Component;
  @ViewChild(WordsBlockComponent, { static: true }) private wordsBlockComponent: WordsBlockComponent;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;

  protected subscriptions: Subscription = new Subscription();
  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.subscriptions.add(
      this.route.queryParams.subscribe((data) => {
        if (data.hasOwnProperty('random')) {
          this.random = data.random;
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }


  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

}
