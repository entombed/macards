import { Component, OnInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';


@Component({
  selector: 'app-daughter-of-father-block01',
  templateUrl: './daughter-of-father-block01.component.html',
  styleUrls: ['./daughter-of-father-block01.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class DaughterOfFatherBlock01Component extends BaseCardBlock implements OnInit {

  public maxCardCount = 55;
  public title = 'FATHER.TITLE';

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'png', 'daughter-of-father', 'daughterOfFatherBlock01Component', '420px', 'shirt.png');
    super.ngOnInit();
  }
}
