import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';
import { DaughterOfFatherRoutingModule } from './daughter-of-father-routing.module';
import { DaughterOfFatherPageComponent } from './daughter-of-father-page.component';
import { DeskModule } from '@components/desk/desk.module';
import { DaughterOfFatherBlock01Component } from './daughter-of-father-block01/daughter-of-father-block01.component';

@NgModule({
  declarations: [
    DaughterOfFatherPageComponent,
    DaughterOfFatherBlock01Component
  ],
  imports: [
    CommonModule,
    DaughterOfFatherRoutingModule,
    ShareModulesModule,
    DeskModule
  ],
  exports: [
    DaughterOfFatherBlock01Component
  ]
})
export class DaughterOfFatherModule { }
