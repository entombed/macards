import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { DaughterOfFatherBlock01Component } from './daughter-of-father-block01/daughter-of-father-block01.component';
import { ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';

@Component({
  selector: 'app-daughter-of-father-page',
  templateUrl: './daughter-of-father-page.component.html',
  styleUrls: ['./daughter-of-father-page.component.css']
})
export class DaughterOfFatherPageComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(DaughterOfFatherBlock01Component, { static: true }) private daughterOfFatherBlock01Component: DaughterOfFatherBlock01Component;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }

  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

}
