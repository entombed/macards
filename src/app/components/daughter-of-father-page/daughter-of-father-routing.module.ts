import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DaughterOfFatherPageComponent } from './daughter-of-father-page.component';


const routes: Routes = [
  { path: 'daughter-of-father', component: DaughterOfFatherPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DaughterOfFatherRoutingModule { }
