import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Card } from '@share/card';

import { DeskComponent } from '@components/desk/desk.component';

import { RobotsBlock01Component } from '@components/robots-page/robots-block-01/robots-block-01.component';
import { RobotsBlock02Component } from '@components/robots-page/robots-block-02/robots-block-02.component';
import { RobotsBlock03Component } from '@components/robots-page/robots-block-03/robots-block-03.component';

import { ScenariosBlock01Component } from '@components/scenarios-page/scenarios-block-01/scenarios-block-01.component';

import { CadBlock01Component } from '@components/commands-and-decisions-page/cad-block-01/cad-block-01.component';
import { CadBlock02Component } from '@components/commands-and-decisions-page/cad-block-02/cad-block-02.component';
import { CadBlock03Component } from '@components/commands-and-decisions-page/cad-block-03/cad-block-03.component';

import { AboutMeBlock01Component } from '@components/about-me-page/about-me-block-01/about-me-block-01.component';
import { AboutMeBlock02Component } from '@components/about-me-page/about-me-block-02/about-me-block-02.component';
import { AboutMeBlock03Component } from '@components/about-me-page/about-me-block-03/about-me-block-03.component';

import { OhBlock01Component } from '@components/oh-page/oh-block-01/oh-block-01.component';
import { OhBlock02Component } from '@components/oh-page/oh-block-02/oh-block-02.component';

import { MerlinBlock01Component } from '@components/merlin-page/merlin-block-01/merlin-block-01.component';

import { AboutYouBlock01Component } from '@components/about-you-page/about-you-block-01/about-you-block-01.component';

import { SgtlBlock01Component } from '@components/say-goodbay-to-live-page/sgtl-block-01/sgtl-block-01.component';
import { SgtlBlock02Component } from '@components/say-goodbay-to-live-page/sgtl-block-02/sgtl-block-02.component';

import { UmbrellasBlock01Component } from '@components/umbrellas-page/umbrellas-block-01/umbrellas-block-01.component';
import { UmbrellasBlock02Component } from '@components/umbrellas-page/umbrellas-block-02/umbrellas-block-02.component';

import { PrismBlock01Component } from '@components/prism-page/prism-block-01/prism-block-01.component';

import { RiverBlock01Component } from '@components/river-page/river-block-01/river-block-01.component';

import { BetweenUsBlock01Component } from '@components/between-us-page/between-us-block-01/between-us-block-01.component';

import { FamilyHistoryBlock01Component } from '@components/family-history-page/family-history-block-01/family-history-block-01.component';

import { SubpersonalityBlock01Component } from '@components/subpersonality-page/subpersonality-block-01/subpersonality-block-01.component';

import { MixedFeelingsBlock01Component } from '@components/mixed-feelings-page/mixed-feelings-block-01/mixed-feelings-block-01.component';

import { MotherBlock01Component } from '@components/mother-page/mother-block-01/mother-block-01.component';
import { MotherBlock02Component } from '@components/mother-page/mother-block-02/mother-block-02.component';

import { LifeScriptsBlock01Component } from '@components/life-scripts-page/life-scripts-block-01/life-scripts-block-01.component';
import { LifeScriptsBlock02Component } from '@components/life-scripts-page/life-scripts-block-02/life-scripts-block-02.component';

import {
  SkillsAndAbilitiesBlock01Component
} from '@components/skills-and-abilities-page/skills-and-abilities-block-01/skills-and-abilities-block-01.component';

import { WordsBlockComponent } from '@components/words-block/words-block.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { CustomMenuComponent } from '@components/custom-page/custom-menu/custom-menu.component';
import {
  DaughterOfFatherBlock01Component
} from '@components/daughter-of-father-page/daughter-of-father-block01/daughter-of-father-block01.component';
import { CockroachesBlock01Component } from '@components/cockroaches/cockroaches-block01/cockroaches-block01.component';
import { DuetBlock01Component } from '@components/duet-page/duet-block01/duet-block01.component';
import { RolesAndArchetypesBlock01Component } from '@components/roles-and-archetypes/roles-and-archetypes-block01/roles-and-archetypes-block01.component';
import { RolesAndArchetypesBlock02Component } from '@components/roles-and-archetypes/roles-and-archetypes-block02/roles-and-archetypes-block02.component';
import { ComplexesPsyDefenseBlock01Component } from '@components/complexes-psy-defense/complexes-psy-defense-block01/complexes-psy-defense-block01.component';
import { ComplexesPsyDefenseBlock02Component } from '@components/complexes-psy-defense/complexes-psy-defense-block02/complexes-psy-defense-block02.component';
import { VitaminsForSoulBlock01Component } from '@components/vitamins-for-soul/vitamins-for-soul-block-01/vitamins-for-soul-block-01.component';

@Component({
  selector: 'app-custom-page',
  templateUrl: './custom-page.component.html',
  styleUrls: ['./custom-page.component.css']
})
export class CustomPageComponent implements OnInit, AfterContentInit {

  constructor(
    private translate: TranslateService
  ) {
    translate.setDefaultLang('ru');
    translate.use('ru');
  }

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;

  @ViewChild(RobotsBlock01Component) private robotsBlock01Component: RobotsBlock01Component;
  @ViewChild(RobotsBlock02Component) private robotsBlock02Component: RobotsBlock02Component;
  @ViewChild(RobotsBlock03Component) private robotsBlock03Component: RobotsBlock03Component;

  @ViewChild(ScenariosBlock01Component) private scenariosBlock01Component: ScenariosBlock01Component;

  @ViewChild(CadBlock01Component) private cadBlock01Component: CadBlock01Component;
  @ViewChild(CadBlock02Component) private cadBlock02Component: CadBlock02Component;
  @ViewChild(CadBlock03Component) private cadBlock03Component: CadBlock03Component;

  @ViewChild(AboutMeBlock01Component) private aboutMeBlock01Component: AboutMeBlock01Component;
  @ViewChild(AboutMeBlock02Component) private aboutMeBlock02Component: AboutMeBlock02Component;
  @ViewChild(AboutMeBlock03Component) private aboutMeBlock03Component: AboutMeBlock03Component;

  @ViewChild(OhBlock01Component) private ohBlock01Component: OhBlock01Component;
  @ViewChild(OhBlock02Component) private ohBlock02Component: OhBlock02Component;

  @ViewChild(MerlinBlock01Component) private merlinBlock01Component: MerlinBlock01Component;

  @ViewChild(AboutYouBlock01Component) private аboutYouBlock01Component: AboutYouBlock01Component;

  @ViewChild(SgtlBlock01Component) private sgtlBlock01Component: SgtlBlock01Component;
  @ViewChild(SgtlBlock02Component) private sgtlBlock02Component: SgtlBlock02Component;

  @ViewChild(UmbrellasBlock01Component) private umbrellasBlock01Component: UmbrellasBlock01Component;
  @ViewChild(UmbrellasBlock02Component) private umbrellasBlock02Component: UmbrellasBlock02Component;

  @ViewChild(PrismBlock01Component) private prismBlock01Component: PrismBlock01Component;

  @ViewChild(RiverBlock01Component) private riverBlock01Component: RiverBlock01Component;

  @ViewChild(BetweenUsBlock01Component) private betweenUsBlock01Component: BetweenUsBlock01Component;

  @ViewChild(FamilyHistoryBlock01Component) private familyHistoryBlock01Component: FamilyHistoryBlock01Component;

  @ViewChild(SubpersonalityBlock01Component) private subpersonalityBlock01Component: SubpersonalityBlock01Component;

  @ViewChild(MixedFeelingsBlock01Component) private mixedFeelingsBlock01Component: MixedFeelingsBlock01Component;

  @ViewChild(MotherBlock01Component) private motherBlock01Component: MotherBlock01Component;
  @ViewChild(MotherBlock02Component) private motherBlock02Component: MotherBlock02Component;

  @ViewChild(LifeScriptsBlock01Component) private lifeScriptsBlock01Component: LifeScriptsBlock01Component;
  @ViewChild(LifeScriptsBlock02Component) private lifeScriptsBlock02Component: LifeScriptsBlock02Component;

  @ViewChild(DaughterOfFatherBlock01Component) private daughterOfFatherBlock01Component: DaughterOfFatherBlock01Component;

  @ViewChild(SkillsAndAbilitiesBlock01Component) private skillsAndAbilitiesBlock01Component: SkillsAndAbilitiesBlock01Component;

  @ViewChild(CockroachesBlock01Component) private cockroachesBlock01Component: CockroachesBlock01Component;

  @ViewChild(DuetBlock01Component) private duetBlock01Component: DuetBlock01Component;

  @ViewChild(RolesAndArchetypesBlock01Component) private rolesAndArchetypesBlock01Component: RolesAndArchetypesBlock01Component;
  @ViewChild(RolesAndArchetypesBlock02Component) private rolesAndArchetypesBlock02Component: RolesAndArchetypesBlock02Component;

  @ViewChild(ComplexesPsyDefenseBlock01Component) private complexesPsyDefenseBlock01Component: ComplexesPsyDefenseBlock01Component;
  @ViewChild(ComplexesPsyDefenseBlock02Component) private complexesPsyDefenseBlock02Component: ComplexesPsyDefenseBlock02Component;

  @ViewChild(VitaminsForSoulBlock01Component) private vitaminsForSoulBlock01Component: VitaminsForSoulBlock01Component;

  @ViewChild(WordsBlockComponent) private wordsBlockComponent: WordsBlockComponent;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;
  @ViewChild(CustomMenuComponent, { static: true }) private customMenuComponent: CustomMenuComponent;

  cardDecks = {
    robotDeck: {
      status: false,
      name: 'ROBOTS.TITLE',
    },
    aboutMeDeck: {
      status: false,
      name: 'ABOUT_ME.TITLE',
    },
    cadDeck: {
      status: false,
      name: 'CAD.TITLE',
    },
    scenarios: {
      name: 'SCENARIOS.TITLE',
      status: false,
    },
    oh: {
      name: 'OH.TITLE',
      status: false,
    },
    merlin: {
      name: 'MERLIN.TITLE',
      status: false,
    },
    aboutYou: {
      name: 'ABOUT_YOU.TITLE',
      status: false,
    },
    sayGoodbye: {
      name: 'SGTL.TITLE',
      status: false,
    },
    umbrellas: {
      name: 'UMBRELLAS.TITLE',
      status: false,
    },
    prism: {
      name: 'PRISM.TITLE',
      status: false,
    },
    river: {
      name: 'RIVER.TITLE',
      status: false,
    },
    betweenUs: {
      name: 'BETWEEN_US.TITLE',
      status: false,
    },
    familyHistory: {
      name: 'FAMILY_HISTORY.TITLE',
      status: false,
    },
    subpersonality: {
      name: 'SUBPERSONALITY.TITLE',
      status: false,
    },
    mixedFeelings: {
      name: 'MIXED_FEELINGS.TITLE',
      status: false,
    },
    mother: {
      name: 'MOTHER.TITLE',
      status: false,
    },
    father: {
      name: 'FATHER.TITLE',
      status: false,
    },
    lifeScripts: {
      name: 'LIFE_SCRIPTS.TITLE',
      status: false,
    },
    skillsAbilities: {
      name: 'SKILLS_ABILITIES.TITLE',
      status: false,
    },
    cockroaches: {
      name: 'COCKROACHES.TITLE',
      status: false,
    },
    duet: {
      name: 'DUET.TITLE',
      status: false,
    },
    rolesAndArchetypes: {
      name: 'ROLES_AND_ARCHETYPES.TITLE',
      status: false,
    },
    fearsBenefits: {
      name: 'FEARS_BENEFITS.TITLE',
      status: false,
    },
    complexesPsyDefense: {
      name: 'COMPLEXES_PSY_DEFENSE.TITLE',
      status: false,
    },
    vitaminsForSoul: {
      name: 'VITAMINS_FOR_SOUL.TITLE',
      status: false,
    },
    words: {
      name: 'ACTIONS',
      status: false,
    },

  };

  ngOnInit() {
  }

  ngAfterContentInit() {
    setTimeout(() => {
      this.showCustomMenu();
    }, 200);
  }

  /**
   * отправляем карточку на стол
   *
   * @author A.Bondarenko
   * @date 2019-12-23
   * @param {*} data
   * @memberof CustomPageComponent
   */
  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  /**
   * возвращаем карту в нужную колоду
   * ориентируемся по data.block (имя\class компонента с маленькой буквы импортировано через @ViewChild)
   * что бы потом вызвать его метод addToArray и передать туда карту
   *
   * @author A.Bondarenko
   * @date 2019-12-23
   * @param {*} data
   * @memberof CustomPageComponent
   */
  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

  showCustomMenu() {
    this.customMenuComponent.show();
  }

}
