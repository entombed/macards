import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-custom-menu',
  templateUrl: './custom-menu.component.html',
  styleUrls: ['./custom-menu.component.css']
})
export class CustomMenuComponent implements OnInit {

  constructor(
    private translate: TranslateService
  ) {
    translate.setDefaultLang('ru');
    translate.use('ru');
  }


  @Input() inCardDecks;
  display = false;

  ngOnInit() {

  }
  show() {
    this.display = true;
  }
}
