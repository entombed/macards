import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CockroachesComponent } from './cockroaches.component';


const routes: Routes = [
  { path: 'cockroaches', component: CockroachesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CockroachesRoutingModule { }
