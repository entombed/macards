import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';

import { DeskModule } from '@components/desk/desk.module';
import { CockroachesBlock01Component } from './cockroaches-block01/cockroaches-block01.component';
import { CockroachesComponent } from './cockroaches.component';
import { CockroachesRoutingModule } from './cockroaches-routing.module';



@NgModule({
  declarations: [
    CockroachesComponent,
    CockroachesBlock01Component
  ],
  imports: [
    CommonModule,
    ShareModulesModule,
    DeskModule,
    CockroachesRoutingModule
  ],
  exports: [
    CockroachesComponent,
    CockroachesBlock01Component
  ]
})
export class CockroachesModule { }
