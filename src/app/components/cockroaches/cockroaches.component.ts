import { Component, OnInit, ViewChild } from '@angular/core';
import { CockroachesBlock01Component } from './cockroaches-block01/cockroaches-block01.component';
import { DeskComponent } from '@components/desk/desk.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';
@Component({
  selector: 'app-cockroaches',
  templateUrl: './cockroaches.component.html',
  styleUrls: ['./cockroaches.component.css']
})
export class CockroachesComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(CockroachesBlock01Component, { static: true }) private cockroachesBlock01Component: CockroachesBlock01Component;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }

  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

}
