import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { ActivatedRoute } from '@angular/router';
import { FearsBenefitsBlock01Component } from './fears-benefits-block01/fears-benefits-block01.component';
import { FearsBenefitsBlock02Component } from './fears-benefits-block02/fears-benefits-block02.component';
import { Card } from '@share/card';

@Component({
  selector: 'app-fears-benefits',
  templateUrl: './fears-benefits.component.html',
  styleUrls: ['./fears-benefits.component.css']
})
export class FearsBenefitsComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(FearsBenefitsBlock01Component, { static: true }) private fearsBenefitsBlock01Component: FearsBenefitsBlock01Component;
  @ViewChild(FearsBenefitsBlock02Component, { static: true }) private fearsBenefitsBlock02Component: FearsBenefitsBlock02Component;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }


  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

}
