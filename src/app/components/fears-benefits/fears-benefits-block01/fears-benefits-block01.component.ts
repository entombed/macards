import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-fears-benefits-block01',
  templateUrl: './fears-benefits-block01.component.html',
  styleUrls: ['./fears-benefits-block01.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class FearsBenefitsBlock01Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 48;
  public words = ['FEARS_BENEFITS.BENEFITS'];

  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['FEARS_BENEFITS.BENEFITS']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(
      this.maxCardCount,
      'jpg',
      'fears-benefits/benefits',
      'fearsBenefitsBlock01Component',
      '300px',
      'shirt.jpg'
    );
    super.ngOnInit();

  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
