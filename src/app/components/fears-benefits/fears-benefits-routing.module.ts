import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FearsBenefitsComponent } from './fears-benefits.component';


const routes: Routes = [
  { path: 'fears-benefits', component: FearsBenefitsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FearsBenefitsRoutingModule { }
