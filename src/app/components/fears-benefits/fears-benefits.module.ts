import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';
import { DeskModule } from '@components/desk/desk.module';

import { FearsBenefitsComponent } from './fears-benefits.component';
import { FearsBenefitsBlock01Component } from './fears-benefits-block01/fears-benefits-block01.component';
import { FearsBenefitsBlock02Component } from './fears-benefits-block02/fears-benefits-block02.component';
import { FearsBenefitsRoutingModule } from './fears-benefits-routing.module';


@NgModule({
  declarations: [
    FearsBenefitsComponent,
    FearsBenefitsBlock01Component,
    FearsBenefitsBlock02Component
  ],
  imports: [
    CommonModule,
    FearsBenefitsRoutingModule,
    ShareModulesModule,
    DeskModule
  ],
  exports: [
    FearsBenefitsComponent,
    FearsBenefitsBlock01Component,
    FearsBenefitsBlock02Component
  ]
})
export class FearsBenefitsModule { }
