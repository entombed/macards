import { Component, OnInit, ViewChild } from '@angular/core';
import { DeskComponent } from '@components/desk/desk.component';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';
import { InputPanelComponent } from '@components/desk/input-panel/input-panel.component';
import { Route, ActivatedRoute } from '@angular/router';
import { Card } from '@share/card';
import { RolesAndArchetypesBlock01Component } from './roles-and-archetypes-block01/roles-and-archetypes-block01.component';
import { RolesAndArchetypesBlock02Component } from './roles-and-archetypes-block02/roles-and-archetypes-block02.component';

@Component({
  selector: 'app-roles-and-archetypes',
  templateUrl: './roles-and-archetypes.component.html',
  styleUrls: ['./roles-and-archetypes.component.css']
})
export class RolesAndArchetypesComponent implements OnInit {

  @ViewChild(DeskComponent, { static: true }) private deskComponent: DeskComponent;
  @ViewChild(
    RolesAndArchetypesBlock01Component, { static: true }
  ) private rolesAndArchetypesBlock01Component: RolesAndArchetypesBlock01Component;
  @ViewChild(
    RolesAndArchetypesBlock02Component, { static: true }
  ) private rolesAndArchetypesBlock02Component: RolesAndArchetypesBlock02Component;
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;
  @ViewChild(InputPanelComponent, { static: true }) private inputPanelComponent: InputPanelComponent;

  public random = false;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.hasOwnProperty('random')) {
        this.random = data.random;
      }
    });
  }


  pushObjectToDesk(data: Card) {
    this.deskComponent.addToArray(data);
  }

  pushObjectBack(data: Card) {
    data.moved = false;
    delete data.type;
    this[data.block].addToArray(data);
  }

  showInputPanel() {
    this.inputPanelComponent.show();
  }

  showDeckMenu() {
    this.deckMenuComponent.show();
  }
}
