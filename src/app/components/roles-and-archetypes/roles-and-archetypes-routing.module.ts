import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RolesAndArchetypesComponent } from './roles-and-archetypes.component';

const routes: Routes = [
  { path: 'roles-and-archetypes', component: RolesAndArchetypesComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesAndArchetypesRoutingModule { }
