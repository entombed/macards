import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModulesModule } from '@share/share-modules/share-modules.module';

import { DeskModule } from '@components/desk/desk.module';
import { RolesAndArchetypesComponent } from './roles-and-archetypes.component';
import { RolesAndArchetypesBlock01Component } from './roles-and-archetypes-block01/roles-and-archetypes-block01.component';
import { RolesAndArchetypesBlock02Component } from './roles-and-archetypes-block02/roles-and-archetypes-block02.component';
import { RolesAndArchetypesRoutingModule } from './roles-and-archetypes-routing.module';


@NgModule({
  declarations: [
    RolesAndArchetypesComponent,
    RolesAndArchetypesBlock01Component,
    RolesAndArchetypesBlock02Component
  ],
  imports: [
    CommonModule,
    ShareModulesModule,
    DeskModule,
    RolesAndArchetypesRoutingModule
  ],
  exports: [
    RolesAndArchetypesComponent,
    RolesAndArchetypesBlock01Component,
    RolesAndArchetypesBlock02Component
  ]
})
export class RolesAndArchetypesModule { }
