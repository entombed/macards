import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { ShuffleService } from '@services/shuffle.service';
import { Card } from '@share/card';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { BaseCardBlock } from '@share/base-components/base-card-block';

@Component({
  selector: 'app-roles-and-archetypes-block02',
  templateUrl: './roles-and-archetypes-block02.component.html',
  styleUrls: ['./roles-and-archetypes-block02.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class RolesAndArchetypesBlock02Component extends BaseCardBlock implements OnInit, AfterContentInit {

  public maxCardCount = 64;
  public words = ['ROLES_AND_ARCHETYPES.TITLE', 'ROLES_AND_ARCHETYPES.WOMEN'];
  constructor(
    protected _shuffleService: ShuffleService,
    protected translate: TranslateService
  ) {
    super(_shuffleService, translate);
    this.getTranslates(this.words)
      .then((data) => {
        this.title = `${data['ROLES_AND_ARCHETYPES.TITLE']} - ${data['ROLES_AND_ARCHETYPES.WOMEN']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.deckCardsArray = this.createDeckCards(this.maxCardCount, 'jpg', 'roles-and-archetypes/women', 'rolesAndArchetypesBlock02Component', '300px', 'shirt.jpg');
    super.ngOnInit();
  }

  ngAfterContentInit() {
    setTimeout(() => this.inverte(this.deckCardsArray), 1000);
  }

}
