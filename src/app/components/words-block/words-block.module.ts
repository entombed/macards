import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WordsBlockComponent } from './words-block.component';
import { ShareModulesModule } from '@share/share-modules/share-modules.module'

@NgModule({
  declarations: [
    WordsBlockComponent
  ],
  imports: [
    CommonModule,
    ShareModulesModule
  ],
  exports: [
    WordsBlockComponent
  ]
})
export class WordsBlockModule { }
