import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AnimationInDeck } from '@share/animation';
import { TranslateService } from '@ngx-translate/core';
import { Card } from '@share/card';

@Component({
  selector: 'app-words-block',
  templateUrl: './words-block.component.html',
  styleUrls: ['./words-block.component.css'],
  animations: [
    AnimationInDeck
  ]
})
export class WordsBlockComponent implements OnInit {
  public title: string = null;
  public words = ['ACTIONS'];

  constructor(
    private translate: TranslateService
  ) {
    this.translate.get(this.words).toPromise()
      .then((data) => {
        this.title = `${data['ACTIONS']}`;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  @Output() outWordToDesk = new EventEmitter<any>();

  public arrayWords: string[] = [
    'Соответствовать', 'Стыдить (ся)', 'Избегать', 'Затягивать', 'Обесценивать', 'Жалеть', 'Сомневаться', 'Запрещать', 'Бояться',
    'Тревожиться', 'Смиряться', 'Выбирать (делать выбор)', 'Улучшать/совершенствовать', 'Жаловаться', 'Бездействовать', 'Пытаться',
    'Подражать/копировать', 'Ограничивать', 'Ухаживать', 'Прятать/скрывать', 'Паниковать', 'Проверять', 'Обвинять', 'Отказывать (ся)',
    'Общаться', 'Лениться', 'Наказывать', 'Мечтать', 'Обижать (ся)', 'Ошибаться', 'Ненавидеть', 'Радовать (ся)', 'Терять', 'Надеяться',
    'Обманывать (ся)', 'Освобождать (ся)', 'Терпеть', 'Настаивать/требовать', 'Молчать', 'Прояснять (вносить ясность)', 'Обещать',
    'Менять (ся)', 'Подчинять (ся)', 'Соревноваться', 'Работать', 'Решать (принимать решение)', 'Баловать', 'Расти/развиваться',
    'Рассчитывать/взвешивать', 'Угрожать', 'Уставать', 'Умирать', 'Учить (ся)', 'Преследовать', 'Стесняться', 'Разочаровывать (ся)',
    'Хотеть', 'Планировать', 'Ревновать', 'Чувствовать', 'Управлять', 'Удивлять (ся)', 'Учитывать', 'Притворяться', 'Хвалить',
    'Шантажировать/ставить условия', 'Сравнивать', 'Удовлетворять', 'Фантазировать/придумывать', 'Прощать', 'Плакать', 'Преувеличивать',
    'Предвосхищать', 'Провоцировать', 'Раздражать (ся)', 'Разрешать', 'Развлекать (ся)', 'Намекать', 'Помогать', 'Мстить', 'Отделять (ся)',
    'Спешить', 'Уважать', 'Нуждаться', 'Заботиться', 'Любить', 'Говорить', 'Организовывать (ся)', 'Инвестировать/вкладывать', 'Страдать',
    'Игнорировать', 'Критиковать/осуждать', 'Контрактировать (заключать договор)', 'Зависеть', 'Клясться', 'Идеализировать', 'Болеть',
    'Бороться', 'Заявлять/выражать', 'Бить/убивать/разрушать', 'Гневаться', 'Возмущаться', 'Делать/действовать', 'Грустить/печалиться',
    'Кричать', 'Дистанцироваться/отдаляться', 'Скучать', 'Казаться (показалось, кажется)', 'Доказывать', 'Манипулировать',
    'Советовать (ся)', 'Творить/создавать', 'Думать', 'Воспитывать', 'Доверять', 'Спасать', 'Контролировать', 'Копить', 'Стараться', 'Жить',
    'Ждать', 'Заставлять', 'Завидовать', 'Жертвовать', 'Дарить', 'Защищать (ся)', 'Восхищать (ся)', 'Богатеть', 'Анализировать', 'Просить'
  ];

  ngOnInit() {
    this.arrayWords = this.arrayWords.map(this.checkCase).sort();
  }

  public checkCase(item) {
    if (typeof (item) !== 'string') {
      return item;
    }
    item = item[0] === item[0].toUpperCase() ? item : item.slice(0, 1).toUpperCase() + item.slice(1);
    return item;
  }

  public onDblclick(word: string) {
    const index = this.arrayWords.indexOf(word);
    const tmpWord = {
      type: 'word',
      name: word,
      id: index,
      moved: false,
      block: 'wordsBlockComponent'
    };
    this.arrayWords.splice(index, 1);
    this.outWordToDesk.emit(tmpWord);
  }

  public addToArray(data: Card) {
    this.arrayWords.splice(data.id, 0, data.name);
    this.arrayWords = this.arrayWords.map(this.checkCase).sort();
  }

}
