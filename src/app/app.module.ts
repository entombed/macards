import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@services/httpLoaderFactory';

import { ShareModulesModule } from '@share/share-modules/share-modules.module';

import { AppComponent } from './app.component';
import { StartPageComponent } from './start-page/start-page.component';
import { CustomPageComponent } from '@components/custom-page/custom-page.component';
import { CustomMenuComponent } from '@components/custom-page/custom-menu/custom-menu.component';

import { ShuffleService } from '@services/shuffle.service';

import { DeskModule } from '@components/desk/desk.module';
import { CommandsAndDecisionsModule } from '@components/commands-and-decisions-page/commands-and-decisions.module';
import { AboutMeModule } from '@components/about-me-page/about-me.module';
import { AboutYouModule } from '@components/about-you-page/about-you.module';
import { BetweenUsModule } from '@components/between-us-page/between-us.module';
import { FamilyHistoryModule } from '@components/family-history-page/family-history.module';
import { MerlinModule } from '@components/merlin-page/merlin.module';
import { MixedFeelingsModule } from '@components/mixed-feelings-page/mixed-feelings.module';
import { OhModule } from '@components/oh-page/oh.module';
import { PrismModule } from '@components/prism-page/prism.module';
import { RiverModule } from '@components/river-page/river.module';
import { RobotsModule } from '@components/robots-page/robots.module';
import { SayGoodbayToLiveModule } from '@components/say-goodbay-to-live-page/say-goodbay-to-live.module';
import { ScenariosModule } from '@components/scenarios-page/scenarios.module';
import { SubpersonalityModule } from '@components/subpersonality-page/subpersonality.module';
import { UmbrellasModule } from '@components/umbrellas-page/umbrellas.module';
import { MotherModule } from '@components/mother-page/mother.module';
import { LifeScriptsModule } from '@components/life-scripts-page/life-scripts.module';
import { SkillsAndAbilitiesModule } from '@components/skills-and-abilities-page/skills-and-abilities.module';
import { DaughterOfFatherModule } from '@components/daughter-of-father-page/daughter-of-father.module';
import { WordsBlockModule } from '@components/words-block/words-block.module';
import { CockroachesModule } from '@components/cockroaches/cockroaches.module';
import { DuetModule } from '@components/duet-page/duet.module';
import { RolesAndArchetypesModule } from '@components/roles-and-archetypes/roles-and-archetypes.module';
import { FearsBenefitsModule } from '@components/fears-benefits/fears-benefits.module';
import { ComplexesPsyDefenseModule } from '@components/complexes-psy-defense/complexes-psy-defense.module';
import { VitaminsForSoulModule } from '@components/vitamins-for-soul/vitamins-for-soul.module';


const appRoutes: Routes = [
  { path: '', component: StartPageComponent },
  { path: 'custom-page', component: CustomPageComponent },
  { path: '**', component: StartPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
    CustomPageComponent,
    CustomMenuComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ShareModulesModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    DeskModule,
    WordsBlockModule,
    CommandsAndDecisionsModule,
    AboutMeModule,
    AboutYouModule,
    BetweenUsModule,
    FamilyHistoryModule,
    MerlinModule,
    MixedFeelingsModule,
    OhModule,
    PrismModule,
    RiverModule,
    RobotsModule,
    SayGoodbayToLiveModule,
    ScenariosModule,
    SubpersonalityModule,
    UmbrellasModule,
    MotherModule,
    LifeScriptsModule,
    DaughterOfFatherModule,
    SkillsAndAbilitiesModule,
    CockroachesModule,
    DuetModule,
    RolesAndArchetypesModule,
    FearsBenefitsModule,
    ComplexesPsyDefenseModule,
    VitaminsForSoulModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    ShuffleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
