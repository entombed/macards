import { Component, OnInit, ViewChild } from '@angular/core';
import { DeckMenuComponent } from '@components/desk/deck-menu/deck-menu.component';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {
  @ViewChild(DeckMenuComponent, { static: true }) private deckMenuComponent: DeckMenuComponent;

  constructor() { }

  ngOnInit() {
  }

  showLinkMenu() {
    this.deckMenuComponent.show();
  }
}
